import {Component, OnInit} from '@angular/core';
import {UserService} from '../../services/user.service';
import {GuestEntity} from '../../model/guest';
import {Router} from '@angular/router';

@Component({
  selector: 'app-account',
  templateUrl: './account.component.html',
  styleUrls: ['./account.component.css']
})
export class AccountComponent implements OnInit {

  username: string;
  password: string;
  pol: string;
  user: GuestEntity;
  guest: GuestEntity;

  constructor(private userService: UserService, private router: Router) {
    this.username = '';
    this.password = '';
    this.user = new GuestEntity();
    this.guest = new GuestEntity();
  }

  ngOnInit() {
  }

  login() {
    this.userService.login(this.username, this.password).subscribe(res => {
      const a = res as GuestEntity;
      localStorage.setItem('loggedUser', JSON.stringify(res));
      switch (res['Role']) {
        case 0:
          this.router.navigate(['/admin']);
          break;

        case 1:
          this.router.navigate(['/guest']);
          break;

        case 2:
          this.router.navigate(['/host']);
          break;
        default:

      }
      // tslint:disable-next-line:no-unused-expression
    }, error1 => {
      alert('Username or password is not correct! Try again.');
    });
  }

  register() {
    if (this.pol === 'Male') {
      this.user.Gender = 0;
    } else {
      this.user.Gender = 1;
      this.user.Show = true;
    }
    this.userService.register(this.user).subscribe(res => {
      this.router.navigate(['/login']);
    });
    this.ngOnInit();
  }
}
