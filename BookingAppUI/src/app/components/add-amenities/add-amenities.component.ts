import {Component, OnInit} from '@angular/core';
import {AmenitiesService} from '../../services/amenities.service';
import {Amenity} from '../../model/amenities';

@Component({
  selector: 'app-add-amenities',
  templateUrl: './add-amenities.component.html',
  styleUrls: ['./add-amenities.component.css']
})
export class AddAmenitiesComponent implements OnInit {

  amenities: Amenity[];
  newAmenity: string;

  // amenities = ['wifi', 'tv', 'parking', 'washing machine', 'kitchen', 'water heater', 'kitchen', 'kitchen', 'kitchen', 'kitchen'];

  constructor(private amenitiesService: AmenitiesService) {
    this.amenities = [];
  }

  ngOnInit() {
    this.getAll();
  }


  getAll() {
    this.amenitiesService.getAll().subscribe(res => {
      this.amenities = res as Amenity[];
    });
  }

  delete(amenity: Amenity) {
    this.amenitiesService.deleteAmenity(amenity.ID).subscribe(res => {
      this.amenities.splice(this.amenities.indexOf(amenity), 1);
    });
  }

  update(amenity: Amenity) {
    this.amenitiesService.update(amenity).subscribe(res => {
    });
  }

  addNewAmenity() {
    const a = new Amenity();
    a.Description = this.newAmenity;
    this.amenitiesService.add(a).subscribe(res => {
      this.amenities.push(res as Amenity);
      this.newAmenity = '';
    });
  }
}
