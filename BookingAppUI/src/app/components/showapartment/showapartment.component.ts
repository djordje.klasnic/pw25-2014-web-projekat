import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {ApartmentService} from '../../services/apartment.service';
import {Apartment} from '../../model/apartment';
import {GuestEntity} from '../../model/guest';
import {NgbActiveModal, NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {Comment} from '../../model/comment';

@Component({
  selector: 'app-showapartment',
  templateUrl: './showapartment.component.html',
  styleUrls: ['./showapartment.component.css']
})
export class ShowapartmentComponent implements OnInit {
  id: string;
  apartment: Apartment;
  loggedUser: GuestEntity;
  newComment: string;
  location: Location;

  constructor(private router: Router, private apartmentService: ApartmentService) {
    this.apartment = new Apartment();
    this.loggedUser = JSON.parse(localStorage.getItem('loggedUser'));
  }

  ngOnInit() {
    this.getForm();
  }

  getForm() {
    this.id = this.router.url.split('/')[2];
    this.apartmentService.getById(this.id).subscribe(res => {
      this.apartment = res as Apartment;
      console.log(res);
    });
  }


  addComment() {
    // const comment = new Comment();
    // comment.Description = this.newComment;
    // this.apartment.Comments.push(comment);
    //  this.apartmentService.addComment(this.apartment.ID, comment).subscribe(app => {
    //   this.apartment = app as Apartment;
    //   console.log(app);
       //this.newComment = '';
    // });
  }

  acceptComment(i: number) {

  }
}
