import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShowapartmentComponent } from './showapartment.component';

describe('ShowapartmentComponent', () => {
  let component: ShowapartmentComponent;
  let fixture: ComponentFixture<ShowapartmentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShowapartmentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShowapartmentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
