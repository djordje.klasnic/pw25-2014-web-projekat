import {Component, OnInit} from '@angular/core';
import {Apartment} from '../../model/apartment';
import {ApartmentService} from '../../services/apartment.service';
import {GuestEntity} from '../../model/guest';
import {Amenity} from '../../model/amenities';
import {AmenitiesService} from '../../services/amenities.service';

@Component({
  selector: 'app-new-apartment',
  templateUrl: './new-apartment.component.html',
  styleUrls: ['./new-apartment.component.css']
})
export class NewApartmentComponent implements OnInit {

  apartment: Apartment;
  public minDate: Date = new Date('09//05/2019');
  public maxDate: Date = new Date('09/25/2019');
  public dateValue: Date = new Date('05/16/2017');
  startDate: Date = new Date();
  endDate: Date = new Date();
  public guest: GuestEntity;
  // amenities = ['wifi', 'tv', 'parking', 'washing machine', 'kitchen', 'water heater', 'kitchen', 'kitchen', 'kitchen', 'kitchen'];
  amenities: Amenity[];
  checked = [];
  signUpTime = {hour: 14, minute: 0};
  checkOutTime = {hour: 22, minute: 0};

  constructor(private apartmentService: ApartmentService, private amenitiesService: AmenitiesService) {
    this.apartment = new Apartment();
    this.amenities = [];
  }

  ngOnInit() {
    this.getAllAmenitites();
  }

  addApartment() {
    const lista = [];
    for (let i = 0; i < this.checked.length; i++) {
      if (this.checked[i]['checked']) {
        this.apartment.Amenities.push(this.amenities[i]);
        lista.push(this.amenities[i]);
      }
    }
    const signUp: Date = new Date();
    signUp.setMinutes(this.signUpTime.minute);
    signUp.setHours(this.signUpTime.hour);
    const checkOut = new Date();
    checkOut.setMinutes(this.checkOutTime.minute);
    checkOut.setHours(this.checkOutTime.hour);
    this.apartment.SignUpTime = signUp;
    this.apartment.CheckOutTime = checkOut;
    const diff = Math.abs(this.endDate.getTime() - this.startDate.getTime());
    const diffDays = Math.ceil(diff / (1000 * 3600 * 24));
    for (let i = 0; i < diffDays; i++) {
      this.apartment.RentDates.push(this.startDate);
    }
    this.guest = JSON.parse(localStorage.getItem('loggedUser'));
    this.apartment.Host = this.guest;
    this.apartment.HostId = this.guest.ID;
    this.apartmentService.addNewApartment(this.apartment).subscribe(result => {
      console.log(result);
    });
  }


  action() {
    console.log(this.checked);
  }

  getAllAmenitites() {
    this.amenitiesService.getAll().subscribe(res => {
      this.amenities = res as Amenity[];
      console.log(this.amenities);
      for (let i = 0; i < this.amenities.length; i++) {
        this.checked.push({'amenity': this.amenities[i], 'checked': false});
      }

    });
  }

}
