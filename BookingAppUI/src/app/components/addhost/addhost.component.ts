import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {GuestEntity} from '../../model/guest';
import {UserService} from '../../services/user.service';
import {AddhostService} from '../../services/addhost.service';

@Component({
  selector: 'app-addhost',
  templateUrl: './addhost.component.html',
  styleUrls: ['./addhost.component.css']
})
export class AddhostComponent implements OnInit {


  pol: string;
  user: GuestEntity;

  constructor(private router: Router, private addhostService: AddhostService) {
    this.user = new GuestEntity();
  }

  ngOnInit() {
  }


  register() {
    if (this.pol === 'Male') {
      this.user.Gender = 0;
    } else {
      this.user.Gender = 1;
    }
    this.addhostService.register(this.user).subscribe(res => {

      this.router.navigate(['/admin']);
    });
  }
}
