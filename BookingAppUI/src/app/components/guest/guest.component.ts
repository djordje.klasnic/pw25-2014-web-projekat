import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {UserService} from '../../services/user.service';
import {GuestEntity} from '../../model/guest';

@Component({
  selector: 'app-guest',
  templateUrl: './guest.component.html',
  styleUrls: ['./guest.component.css']
})
export class GuestComponent implements OnInit {
  user: GuestEntity;

  constructor(private router: Router, private userService: UserService) {
  }

  ngOnInit() {
    this.user = new GuestEntity();
    this.user = JSON.parse(localStorage.getItem('loggedUser')) as GuestEntity;

  }

  EditData() {
    this.router.navigate(['/editdata']);
  }

  Logout() {
    localStorage.clear();
    this.router.navigate(['/login']);
  }

  DeleteAccount() {
    this.userService.DeleteGuest(this.user).subscribe(res => {
      this.router.navigate(['/login']);
    });
  }
}
