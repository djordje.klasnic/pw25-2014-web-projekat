import {Component, OnInit} from '@angular/core';
import {GuestEntity} from '../../model/guest';
import {Router} from '@angular/router';

@Component({
  selector: 'app-nav-bar',
  templateUrl: './nav-bar.component.html',
  styleUrls: ['./nav-bar.component.css']
})
export class NavBarComponent implements OnInit {

  user: GuestEntity;

  constructor(private router: Router) {
    this.user = JSON.parse(localStorage.getItem('loggedUser'));
  }

  ngOnInit() {
  }

  logout() {
    localStorage.clear();
    this.router.navigate(['/login']);
  }


}
