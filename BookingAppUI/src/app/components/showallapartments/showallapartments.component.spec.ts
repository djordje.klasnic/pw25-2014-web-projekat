import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShowallapartmentsComponent } from './showallapartments.component';

describe('ShowallapartmentsComponent', () => {
  let component: ShowallapartmentsComponent;
  let fixture: ComponentFixture<ShowallapartmentsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShowallapartmentsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShowallapartmentsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
