import {Component, Directive, EventEmitter, Input, OnInit, Output, QueryList, ViewChildren} from '@angular/core';
import {GuestEntity} from '../../model/guest';
import {Apartment} from '../../model/apartment';
import {Router} from '@angular/router';
import {ApartmentService} from '../../services/apartment.service';

export type SortDirection = 'asc' | 'desc' | '';
const rotate: { [key: string]: SortDirection } = {'asc': 'desc', 'desc': '', '': 'asc'};
export const compare = (v1, v2) => v1 < v2 ? -1 : v1 > v2 ? 1 : 0;

export interface SortEvent {
  column: string;
  direction: SortDirection;
}

@Directive({
  selector: 'th[sortable]',
  host: {
    '[class.asc]': 'direction === "asc"',
    '[class.desc]': 'direction === "desc"',
    '(click)': 'rotate()'
  }
})
export class NgbdSortableHeader {

  @Input() sortable: string;
  @Input() direction: SortDirection = '';
  @Output() sort = new EventEmitter<SortEvent>();

  rotate() {
    this.direction = rotate[this.direction];
    this.sort.emit({column: this.sortable, direction: this.direction});
  }
}
@Component({
  selector: 'app-showallapartments',
  templateUrl: './showallapartments.component.html',
  styleUrls: ['./showallapartments.component.css']
})
export class ShowallapartmentsComponent implements OnInit {
  apartments: Apartment[];
  user: GuestEntity;
  total = 0;
  page = 0;
  limit = 20;
  apartment: Apartment;
  @ViewChildren(NgbdSortableHeader) headers: QueryList<NgbdSortableHeader>;

  constructor(private router: Router, private apartmentService: ApartmentService) {
  }

  ngOnInit() {
    this.user = new GuestEntity();
    this.user = JSON.parse(localStorage.getItem('loggedUser')) as GuestEntity;

    if (this.user.Role === 0) {
      this.ReadApartments();
    } else if (this.user.Role === 2 && this.router.url.includes('non-active')) {
      this.readHostNonActiveApartments();
    } else if (this.user.Role === 2 && this.router.url.includes('active')) {
      this.readHostActiveApartments();
    } else {
      this.readActiveApartments();
    }
  }

  ReadApartments() {
    this.apartmentService.ReadApartments().subscribe(res => {

      this.apartments = res as Apartment[];
    });
  }

  ShowMore(ID: number) {
    this.router.navigate(['/show-apartment/' + ID]);
  }

  private readHostActiveApartments() {
    this.apartmentService.getHostActiveApartmants(this.user.ID).subscribe(res => {

      this.apartments = res as Apartment[];
    });
  }

  private readHostNonActiveApartments() {
    this.apartmentService.getHostNonActiveApartmants(this.user.ID).subscribe(res => {

      this.apartments = res as Apartment[];
    });
  }

  private readActiveApartments() {
    this.apartmentService.getActiveAparmtments().subscribe(res => {

      this.apartments = res as Apartment[];
    });
  }

  onSort({column, direction}: SortEvent) {

    // resetting other headers
    this.headers.forEach(header => {
      if (header.sortable !== column) {
        header.direction = '';
      }
    });

    // sorting countries
    if (direction === '') {
      this.apartments = this.apartments;
    } else {
      this.apartments = [...this.apartments].sort((a, b) => {
        const res = compare(a[column], b[column]);
        return direction === 'asc' ? res : -res;
      });
    }
  }
}
