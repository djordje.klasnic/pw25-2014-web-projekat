import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {GuestEntity} from '../../model/guest';
import {UserService} from '../../services/user.service';

@Component({
  selector: 'app-host',
  templateUrl: './host.component.html',
  styleUrls: ['./host.component.css']
})
export class HostComponent implements OnInit {
  user: GuestEntity;
  constructor(private router: Router,private userService: UserService) { }

  ngOnInit() {
    this.user = new GuestEntity();
    this.user = JSON.parse(localStorage.getItem('loggedUser')) as GuestEntity;
  }

  EditData(){
    this.router.navigate(['/editdata']);
  }

  Logout(){
    localStorage.clear();
    this.router.navigate(['/login']);
  }

  DeleteAccount() {
    this.userService.DeleteHost(this.user).subscribe(res => {

      this.router.navigate(['/login']);
    });
  }
}
