import { Component, OnInit } from '@angular/core';
import {GuestEntity} from '../../model/guest';
import {Router} from '@angular/router';
import {AddhostService} from '../../services/addhost.service';
import {EditdataService} from '../../services/editdata.service';

@Component({
  selector: 'app-editdata',
  templateUrl: './editdata.component.html',
  styleUrls: ['./editdata.component.css']
})
export class EditdataComponent implements OnInit {


  pol: string;
  user: GuestEntity;

  constructor(private router: Router, private editdataservice: EditdataService) {

  }

  ngOnInit() {
    this.user = new GuestEntity();
    this.user = JSON.parse(localStorage.getItem('loggedUser')) as GuestEntity;

    if (this.user.Gender === 0) {
      this.pol = 'Male';
    } else {
      this.pol = 'Female';
    }
  }


  Edit() {
      if (this.pol === 'Male') {
        this.user.Gender = 0;
      } else {
        this.user.Gender = 1;
      }
      switch (this.user.Role) {
      case 0:
        this.editdataservice.editAdmin(this.user).subscribe(res => {

          localStorage.setItem('loggedUser', JSON.stringify(res));
          this.router.navigate(['/admin'] ); }) ;
        break;
      case 1:
        this.editdataservice.editGuest(this.user).subscribe(res => {

          localStorage.setItem('loggedUser', JSON.stringify(res));
          this.router.navigate(['/guest'] ); }) ;
        break;
        case 2:
        this.editdataservice.editHost(this.user).subscribe ( res => {

          localStorage.setItem('loggedUser', JSON.stringify(res));
          this.router.navigate(['/host'] ); }) ;
        break;
      default:
        }
      }
}
