import {Component, Directive, EventEmitter, Input, OnInit, Output, QueryList, ViewChildren} from '@angular/core';
import {Router} from '@angular/router';
import {UserService} from '../../services/user.service';
import {GuestEntity} from '../../model/guest';


export type SortDirection = 'asc' | 'desc' | '';
const rotate: { [key: string]: SortDirection } = {'asc': 'desc', 'desc': '', '': 'asc'};
export const compare = (v1, v2) => v1 < v2 ? -1 : v1 > v2 ? 1 : 0;

export interface SortEvent {
  column: string;
  direction: SortDirection;
}

@Directive({
  selector: 'th[sortable]',
  host: {
    '[class.asc]': 'direction === "asc"',
    '[class.desc]': 'direction === "desc"',
    '(click)': 'rotate()'
  }
})
export class NgbdSortableHeader {

  @Input() sortable: string;
  @Input() direction: SortDirection = '';
  @Output() sort = new EventEmitter<SortEvent>();

  rotate() {
    this.direction = rotate[this.direction];
    this.sort.emit({column: this.sortable, direction: this.direction});
  }
}


@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css']
})
export class AdminComponent implements OnInit {

  user: GuestEntity;
  users: GuestEntity[];
  total = 0;
  page = 0;
  limit = 20;

  @ViewChildren(NgbdSortableHeader) headers: QueryList<NgbdSortableHeader>;

  constructor(private router: Router, private userService: UserService) {
  }

  ngOnInit() {
    this.user = new GuestEntity();
    this.user = JSON.parse(localStorage.getItem('loggedUser')) as GuestEntity;
    this.ReadAllAdmins();
  }

  ReadAllAdmins() {
    this.userService.ReadAdmins().subscribe(res => {
      this.users = res as GuestEntity[];
      for (let i = 0; i < this.users.length; i++) {
        if (this.users[i].Username === this.user.Username) {
          this.users.splice(i, 1);
        }
      }
    });
  }

  DeleteAccount() {
    this.userService.DeleteAdmin(this.user).subscribe(res => {
      this.router.navigate(['/login']);
    });
  }

  delete(guest: GuestEntity) {
    switch (guest.Role) {
      case 0: {
        this.userService.DeleteAdmin(guest).subscribe(res => {
        });
        break;
      }

      case 1: {
        this.userService.DeleteGuest(guest).subscribe(res => {

        });
        break;
      }
      case 2: {
        this.userService.DeleteHost(guest).subscribe(res => {

        });
        break;
      }
      default:
    }
    this.users.splice(this.users.indexOf(guest), 1);
  }


  onSort({column, direction}: SortEvent) {

    // resetting other headers
    this.headers.forEach(header => {
      if (header.sortable !== column) {
        header.direction = '';
      }
    });

    // sorting countries
    if (direction === '') {
      this.users = this.users;
    } else {
      this.users = [...this.users].sort((a, b) => {
        const res = compare(a[column], b[column]);
        return direction === 'asc' ? res : -res;
      });
    }
  }

}
