import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { AccountComponent } from './components/account/account.component';
import {UserService} from './services/user.service';
import {FormsModule} from '@angular/forms';
import {AppRoutingModule} from './app-routing.module';
import {HttpClientModule} from '@angular/common/http';
import {AdminComponent, NgbdSortableHeader} from './components/admin/admin.component';
import { HostComponent } from './components/host/host.component';
import { GuestComponent } from './components/guest/guest.component';
import { AddhostComponent } from './components/addhost/addhost.component';
import { EditdataComponent } from './components/editdata/editdata.component';
import { ShowallapartmentsComponent } from './components/showallapartments/showallapartments.component';
import {EditdataService} from './services/editdata.service';
import {ApartmentService} from './services/apartment.service';
import { ShowapartmentComponent } from './components/showapartment/showapartment.component';
import { NavBarComponent } from './components/nav-bar/nav-bar.component';
import { NewApartmentComponent } from './components/new-apartment/new-apartment.component';
import { DatePickerModule } from '@syncfusion/ej2-angular-calendars';
import {AmenitiesService} from './services/amenities.service';
import { AddAmenitiesComponent } from './components/add-amenities/add-amenities.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

@NgModule({
  declarations: [
    AppComponent,
    AccountComponent,
    AdminComponent,
    HostComponent,
    GuestComponent,
    AddhostComponent,
    EditdataComponent,
    ShowallapartmentsComponent,
    ShowapartmentComponent,
    NavBarComponent,
    NewApartmentComponent,
    NgbdSortableHeader,
    AddAmenitiesComponent
  ],
  imports: [
    HttpClientModule,
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    DatePickerModule,
    NgbModule
  ],
  providers: [UserService, ApartmentService, EditdataService, AmenitiesService],
  bootstrap: [AppComponent]
})
export class AppModule { }
