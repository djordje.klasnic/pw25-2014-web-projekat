import { Injectable } from '@angular/core';
import {GuestEntity} from '../model/guest';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class AddhostService {

  constructor(private http: HttpClient) { }

  register(guest: GuestEntity) {
    return this.http.post('/api/addhost', guest);
  }
}
