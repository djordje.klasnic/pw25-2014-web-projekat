import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Apartment} from '../model/apartment';

@Injectable({
  providedIn: 'root'
})
export class ApartmentService {
  constructor(private http: HttpClient) {
  }

  ReadApartments() {
    return this.http.get('/api/getapartments');
  }

  writeApp(apartment: Apartment) {
    return this.http.post('/api/postapartment', apartment);
  }

  getById(id: string) {
    return this.http.get('/api/getbyid/' + id);
  }

  getHostActiveApartmants(id: number) {
    return this.http.get('/api/apartments/active/host/' + id);
  }

  getHostNonActiveApartmants(id: number) {
    return this.http.get('/api/apartments/non-active/host/' + id);
  }

  getActiveAparmtments() {
    return this.http.get('/api/apartments/active');
  }

  getByIdUser(id: string) {
    return this.http.get('/api/getbyiduser/' + id);
  }

  addNewApartment(apartment: Apartment) {
    console.log(apartment);
    return this.http.post('/api/add-apartments',
      {
        GuestNumber: apartment.GuestNumber,
        RoomNumber: apartment.RoomNumber,
        Location: apartment.Location,
        Price: apartment.Price,
        Status: 0,
        Type: apartment.Type,
        Host: apartment.Host,
        HostId: apartment.HostId,
        RentDates: apartment.RentDates,
        SignUpTime: apartment.SignUpTime,
        CheckOutTime: apartment.CheckOutTime,
        Amenities: apartment.Amenities
      });

  }

  addComment(ID: number, comment: Comment) {
    return this.http.put('/api/apartment/' + ID + '/comment', comment);

  }

  getLocation(ID: number) {
     return this.http.get('/api/location' + ID);
  }
}
