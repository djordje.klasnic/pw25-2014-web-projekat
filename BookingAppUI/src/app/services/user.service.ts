import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {GuestEntity} from '../model/guest';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private http: HttpClient) {
  }

  login(username: string, password: string) {
    return this.http.post('/api/login', {username, password});
  }

  register(guest: GuestEntity) {
    return this.http.post('/api/register', guest);
  }

  DeleteAdmin(guest: GuestEntity) {
    return this.http.delete('/api/deleteadmin/' + guest.ID);
  }

  DeleteGuest(guest: GuestEntity) {
    return this.http.delete('/api/deleteguest/' + guest.ID);
  }
  DeleteHost(guest: GuestEntity) {
    return this.http.delete('/api/deletehost/' + guest.ID);
  }

  ReadAdmins() {
    return this.http.get('/api/returnallusers');
  }

}
