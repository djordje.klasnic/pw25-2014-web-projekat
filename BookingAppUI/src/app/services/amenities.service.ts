import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Amenity} from '../model/amenities';

@Injectable({
  providedIn: 'root'
})
export class AmenitiesService {

  constructor(private http: HttpClient) {
  }

  getAll() {
    return this.http.get('/api/amenities');
  }

  deleteAmenity(id: number) {
    return this.http.delete('/api/amenities/' + id);
  }

  update(amenity: Amenity) {
    return this.http.put('/api/amenities', amenity);
  }

  add(amenity: Amenity) {
    return this.http.post('/api/amenities', amenity);
  }
}
