import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {GuestEntity} from '../model/guest';

@Injectable({
  providedIn: 'root'
})
export class EditdataService {

  constructor(private http: HttpClient) { }

  editAdmin(guest: GuestEntity) {
    return this.http.put('/api/editadmin', guest);
  }

  editGuest(guest: GuestEntity) {
    return this.http.put('/api/editguest', guest);
  }

  editHost(guest: GuestEntity) {
    return this.http.put('/api/edithost', guest);
  }
}
