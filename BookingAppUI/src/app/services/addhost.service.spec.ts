import { TestBed } from '@angular/core/testing';

import { AddhostService } from './addhost.service';

describe('AddhostService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AddhostService = TestBed.get(AddhostService);
    expect(service).toBeTruthy();
  });
});
