import { TestBed } from '@angular/core/testing';

import { EditdataService } from './editdata.service';

describe('EditdataService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: EditdataService = TestBed.get(EditdataService);
    expect(service).toBeTruthy();
  });
});
