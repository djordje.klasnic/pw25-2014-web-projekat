export class Addresss {
  constructor(public ID?: number,
              public Street?: string,
              public City?: string,
              public ZipCode?: number,
              public Show?: boolean
  ) {
  }
}
