import {Addresss} from './address';
import {Apartment} from './apartment';
import {GuestEntity} from './guest';

export class Reservation {

  constructor(public ID?: number,
              public ReservedApartment?: Apartment,
              public CheckInDate?: Date,
              public NumberOfNights?: number,
              public TotalPrice?: number,
              public guest?: GuestEntity,
              public StatusOFReservation?: number,
              public Show?: boolean
  ) {
    this.ReservedApartment = new Apartment();
    this.CheckInDate = new Date();
    this.guest = new GuestEntity();
  }
}
