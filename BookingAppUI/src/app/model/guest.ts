export class GuestEntity {

  constructor(public ID?: number,
              public Username?: string,
              public Password?: string,
              public FirstName?: string,
              public LastName?: string,
              public Gender?: number,
              public Role?: number,
              public Show?: boolean
  ) {

  }
}
