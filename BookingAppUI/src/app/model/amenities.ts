export class Amenity{
  constructor(public ID?: number,
              public Description?: string,
              public Show?: boolean
  ) {
  }
}
