import {GuestEntity} from './guest';
import {Apartment} from './apartment';

export class Comment {
  constructor(public ID?: number,
              public Description?: string,
              public guest?: GuestEntity,
              public apartment?: Apartment,
              public evaluation?: number,
              public Show?: boolean,
  ) {
    this.guest = new GuestEntity();
    this.apartment = new Apartment();
  }
}
