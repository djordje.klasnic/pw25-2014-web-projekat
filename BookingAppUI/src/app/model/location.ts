import {Addresss} from './address';

export class Locations {

  constructor(public ID?: number,
              public Address?: Addresss,
              public XCoordinate?: number,
              public YCoordinate?: number,
              public Show?: boolean
  ) {
      this.Address = new Addresss();
  }

}
