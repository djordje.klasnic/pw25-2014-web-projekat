import {Comment} from './comment';
import {GuestEntity} from './guest';
import { Amenity} from './amenities';
import {Reservation} from './reservation';

import DateTimeFormat = Intl.DateTimeFormat;
import {Locations} from './location';


export class Apartment {
  constructor(public ID?: number,
              public Type?: number,
              public RoomNumber?: number,
              public GuestNumber?: number,
              public Location?: Locations,
              public RentDates?: Date[],
              public AvailableRentDates?: Date,
              public Host?: GuestEntity,
              public HostId?: number,
              public Comments?: Comment[],
              public Pictures?: string[],
              public Price?: number,
              public SignUpTime?: Date,
              public CheckOutTime?: Date,
              public Status?: number,
              public Amenities?: Amenity[],
              public Reservations?: Reservation[],
              public Show?: boolean,
  ) {
    this.RentDates = [];
    this.Location = new Locations();
    this.AvailableRentDates = new Date();
    this.Host = new GuestEntity();
    this.Comments = [];
    this.Amenities = [];
    this.Reservations = [];
  }


}

