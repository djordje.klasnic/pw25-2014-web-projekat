import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {AccountComponent} from './components/account/account.component';
import {AdminComponent} from './components/admin/admin.component';
import {AddhostComponent} from './components/addhost/addhost.component';
import {HostComponent} from './components/host/host.component';
import {GuestComponent} from './components/guest/guest.component';
import {EditdataComponent} from './components/editdata/editdata.component';
import {ShowallapartmentsComponent} from './components/showallapartments/showallapartments.component';
import {ShowapartmentComponent} from './components/showapartment/showapartment.component';
import {NewApartmentComponent} from './components/new-apartment/new-apartment.component';
import {AddAmenitiesComponent} from './components/add-amenities/add-amenities.component';

const appRoutes: Routes = [
  {path: '', redirectTo: 'login', pathMatch: 'full'},

  {path: 'login', component: AccountComponent},
  {path: 'admin', component: AdminComponent},
  {path: 'edit-data', component: EditdataComponent},
  {path: 'host', component: HostComponent},
  {path: 'guest', component: GuestComponent},
  {path: 'add-host', component: AddhostComponent},
  {path: 'show-all', component: ShowallapartmentsComponent},
  {path: 'active-apartments', component: ShowallapartmentsComponent},
  {path: 'non-active-apartments', component: ShowallapartmentsComponent},
  {path: 'show-apartment/:ID', component: ShowapartmentComponent},
  {path: 'new-apartment', component: NewApartmentComponent},
  {path: 'amenities', component: AddAmenitiesComponent}


  // {path: 'home-page', component: AccountComponent},

];

@NgModule({
  imports: [RouterModule.forRoot(appRoutes, {
    enableTracing: false,
  })],
  exports: [RouterModule]
})

export class AppRoutingModule {
}
