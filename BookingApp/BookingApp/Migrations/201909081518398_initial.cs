namespace BookingApp.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Addresses",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Street = c.String(),
                        City = c.String(),
                        ZipCode = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.Admins",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Username = c.String(),
                        Password = c.String(),
                        FirstName = c.String(),
                        LastName = c.String(),
                        Gender = c.Int(nullable: false),
                        Role = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.Amenities",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Description = c.String(),
                        ApartmentId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Apartments", t => t.ApartmentId, cascadeDelete: true)
                .Index(t => t.ApartmentId);
            
            CreateTable(
                "dbo.Apartments",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Type = c.Int(nullable: false),
                        RoomNumber = c.Int(nullable: false),
                        GuestNumber = c.Int(nullable: false),
                        HostId = c.Int(nullable: false),
                        Price = c.Int(nullable: false),
                        SignUpTime = c.DateTime(nullable: false),
                        CheckOutTime = c.DateTime(nullable: false),
                        Status = c.Int(nullable: false),
                        Location_Id = c.Int(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Hosts", t => t.HostId, cascadeDelete: true)
                .ForeignKey("dbo.Locations", t => t.Location_Id)
                .Index(t => t.HostId)
                .Index(t => t.Location_Id);
            
            CreateTable(
                "dbo.Comments",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Description = c.String(),
                        ApartmentId = c.Int(nullable: false),
                        Evaluation = c.Int(nullable: false),
                        Status = c.Int(nullable: false),
                        Guest_ID = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Apartments", t => t.ApartmentId, cascadeDelete: true)
                .ForeignKey("dbo.Guests", t => t.Guest_ID)
                .Index(t => t.ApartmentId)
                .Index(t => t.Guest_ID);
            
            CreateTable(
                "dbo.Guests",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Username = c.String(),
                        Password = c.String(),
                        FirstName = c.String(),
                        LastName = c.String(),
                        Gender = c.Int(nullable: false),
                        Role = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.Reservations",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        CheckInDate = c.DateTime(nullable: false),
                        NumberOfNights = c.Int(nullable: false),
                        TotalPrice = c.Int(nullable: false),
                        GuestId = c.Int(nullable: false),
                        Status = c.Int(nullable: false),
                        ReservedApartment_ID = c.Int(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Guests", t => t.GuestId, cascadeDelete: true)
                .ForeignKey("dbo.Apartments", t => t.ReservedApartment_ID)
                .Index(t => t.GuestId)
                .Index(t => t.ReservedApartment_ID);
            
            CreateTable(
                "dbo.Hosts",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Username = c.String(),
                        Password = c.String(),
                        FirstName = c.String(),
                        LastName = c.String(),
                        Gender = c.Int(nullable: false),
                        Role = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.Locations",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        XCoordinate = c.Int(nullable: false),
                        YCoordinate = c.Int(nullable: false),
                        Address_ID = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Addresses", t => t.Address_ID)
                .Index(t => t.Address_ID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Amenities", "ApartmentId", "dbo.Apartments");
            DropForeignKey("dbo.Apartments", "Location_Id", "dbo.Locations");
            DropForeignKey("dbo.Locations", "Address_ID", "dbo.Addresses");
            DropForeignKey("dbo.Apartments", "HostId", "dbo.Hosts");
            DropForeignKey("dbo.Comments", "Guest_ID", "dbo.Guests");
            DropForeignKey("dbo.Reservations", "ReservedApartment_ID", "dbo.Apartments");
            DropForeignKey("dbo.Reservations", "GuestId", "dbo.Guests");
            DropForeignKey("dbo.Comments", "ApartmentId", "dbo.Apartments");
            DropIndex("dbo.Locations", new[] { "Address_ID" });
            DropIndex("dbo.Reservations", new[] { "ReservedApartment_ID" });
            DropIndex("dbo.Reservations", new[] { "GuestId" });
            DropIndex("dbo.Comments", new[] { "Guest_ID" });
            DropIndex("dbo.Comments", new[] { "ApartmentId" });
            DropIndex("dbo.Apartments", new[] { "Location_Id" });
            DropIndex("dbo.Apartments", new[] { "HostId" });
            DropIndex("dbo.Amenities", new[] { "ApartmentId" });
            DropTable("dbo.Locations");
            DropTable("dbo.Hosts");
            DropTable("dbo.Reservations");
            DropTable("dbo.Guests");
            DropTable("dbo.Comments");
            DropTable("dbo.Apartments");
            DropTable("dbo.Amenities");
            DropTable("dbo.Admins");
            DropTable("dbo.Addresses");
        }
    }
}
