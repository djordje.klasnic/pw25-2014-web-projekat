using BookingApp.Repository;
using BookingApp.Repository.RepositoryImpl;
using System;
using System.Web.Http;
using Unity;
using Unity.WebApi;

namespace BookingApp
{
    public static class UnityConfig
    { 
    //    #region Unity Container
    //private static Lazy<IUnityContainer> container =
    //      new Lazy<IUnityContainer>(() =>
    //      {
    //          var container = new UnityContainer();
    //          RegisterTypes(container);
    //          return container;
    //      });

    ///// <summary>
    ///// Configured Unity Container.
    ///// </summary>
    //public static IUnityContainer Container => container.Value;
    //#endregion
    public static void Register()
        {
            //container.RegisterType<IAdminRepository, AdminRepository>();
            //container.RegisterType<IHostRepository, HostRepository>();
            //container.RegisterType<IGuestRepository, GuestRepository>();
            // register all your components with the container here
            // it is NOT necessary to register your controllers
            var container = new UnityContainer();
            container.RegisterType<IAdminRepository, AdminRepository>();
            container.RegisterType<IHostRepository, HostRepository>();
            container.RegisterType<IGuestRepository, GuestRepository>();
            container.RegisterType<IApartmentRepository, ApartmentRepository>();
            container.RegisterType<ICommentRepository, CommentRepository>();
            container.RegisterType<IAmenitiesRepository, AmenitiesRepository>();
            container.RegisterType<ILocationRepository, LocationRepository>();
            GlobalConfiguration.Configuration.DependencyResolver = new UnityDependencyResolver(container);
        }
    }
}