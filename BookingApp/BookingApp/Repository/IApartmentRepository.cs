﻿using BookingApp.Models.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BookingApp.Repository
{
    public interface IApartmentRepository
    {
        List<Apartment> ReadAll();
        List<Apartment> ReadActive();

        List<Apartment> ReadHostApartments(int id);
        List<Apartment> ReadActiveHostApartments(int id);
        List<Apartment> ReadNonActiveHostApartments(int id);
        Apartment WriteApartment(Apartment apartment);
        Apartment EditApartment(Apartment apartment);
        Apartment RemoveApartment(Apartment apartment);
        Apartment getByid(int iD);
        Apartment addComment(int iD, Comment comment);
        Comment EditComment(Comment comment);
    }
}
