﻿using BookingApp.Models.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BookingApp.Repository
{
    public interface ILocationRepository
    {
        List<Location> ReadAll();
        Location GetById(string ID);
    }
}
