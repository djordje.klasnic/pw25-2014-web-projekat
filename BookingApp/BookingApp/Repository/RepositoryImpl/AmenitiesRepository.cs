﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BookingApp.Models.Entity;

namespace BookingApp.Repository.RepositoryImpl
{
    public class AmenitiesRepository : IAmenitiesRepository
    {

        Context.Context context;
        public AmenitiesRepository(Context.Context con)
        {
            context = con;
        }

        public Amenities AddAmenities(Amenities amen)
        {
            try
            {
                Amenities a = context.Amenities.Add(amen);
                return a;
            }
            catch
            {
                return null;
            }
        }

        public Amenities Delete(int id)
        {
            try
            {
                Amenities a = context.Amenities.Find(id);
                context.Amenities.Remove(a);
                context.SaveChanges();
                return a;
            }
            catch
            {
                return null;
            }
        }

        public Amenities EditAmenities(Amenities amen)
        {
            try
            {
                context.Amenities.Attach(amen);
                context.Entry(amen).State = System.Data.Entity.EntityState.Modified;
                context.SaveChanges();
                return amen;
            }
            catch
            {
                return null;
            }
        }

        public List<Amenities> ReadAll()
        {
            return context.Amenities.ToList();
        }
    }
}