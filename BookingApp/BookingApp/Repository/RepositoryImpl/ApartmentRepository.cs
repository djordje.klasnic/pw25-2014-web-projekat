﻿using BookingApp.Models.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BookingApp.Repository
{
    public class ApartmentRepository:IApartmentRepository
    {
        Context.Context context;

        public ApartmentRepository(Context.Context ct)
        {
            context = ct;
        }

        public List<Apartment> ReadAll()
        {
            return context.Apartments.ToList();
        }


        public Apartment getByid(int iD)
        {
            
            Apartment ap = context.Apartments.Include("Amenities").Where(w => w.ID == iD).FirstOrDefault();
            return ap;
        }

        public Apartment getByidUser(int iD)
        {
            Apartment a = context.Apartments.Include("User").Where(w => w.ID == iD).FirstOrDefault();
            return a;
        }


        public List<Apartment> ReadActive()
        {
            List<Apartment> list = context.Apartments.ToList();
            List<Apartment> activeList = new List<Apartment>();
            foreach(Apartment a in list)
            {
                if (a.Status == Status.Active)
                {
                    activeList.Add(a);
                }
            }
            return activeList;
        }

        public List<Apartment> ReadHostApartments(int id)
        {

            List<Apartment> list = context.Apartments.ToList();
            List<Apartment> Hostlist = new List<Apartment>();
            foreach (Apartment a in list)
            {
                if (a.Host.ID == id )
                {
                    Hostlist.Add(a);
                }
            }
            return Hostlist;
        }

        public List<Apartment> ReadActiveHostApartments(int id)
        {
            List<Apartment> list = ReadHostApartments(id);
            List<Apartment> Hostlist = new List<Apartment>();
            foreach (Apartment a in list)
            {
                if (a.Status == Status.Active)
                {
                    Hostlist.Add(a);
                }
            }
            return Hostlist;
        }

        public List<Apartment> ReadNonActiveHostApartments(int id)
        {
            List<Apartment> list = ReadHostApartments(id);
            List<Apartment> Hostlist = new List<Apartment>();
            foreach (Apartment a in list)
            {
                if (a.Status == Status.Inactive)
                {
                    Hostlist.Add(a);
                }
            }
            return Hostlist;
        }

        
        public Apartment WriteApartment(Apartment apartment)
        {
            try
            {
                Apartment forBase = new Apartment();
                forBase.SignUpTime = DateTime.Now;
                forBase.CheckOutTime = DateTime.Now;
                forBase.Status = 0;
                forBase.Location = apartment.Location;
                forBase.Price = apartment.Price;
                forBase.RoomNumber = apartment.RoomNumber;
                forBase.GuestNumber = apartment.GuestNumber;
                forBase.Type = apartment.Type;

                List<Host> list = context.Hosts.ToList();
                foreach(Host h in list)
                {
                    if (h.ID == apartment.HostId)
                    {
                        forBase.Host = h;
                        forBase.HostId = h.ID;
                        break;
                    }
                }
                
                Apartment AddApartment = context.Apartments.Add(forBase);


                context.SaveChanges();
                return AddApartment;
            }
            catch
            {
                return null;
            }
        }

        public Apartment EditApartment(Apartment apartment)
        {
            try
            {
                context.Apartments.Attach(apartment);
                context.Entry(apartment).State = System.Data.Entity.EntityState.Modified;
                context.SaveChanges();
                return apartment;
            }
            catch
            {
                return null;
            }
        }

        public Apartment RemoveApartment(Apartment apartment)
        {
            try
            {
                Apartment a= context.Apartments.Remove(apartment);
                return a;
            }
            catch
            {
                return null;
            }
        }

        public Apartment addComment(int iD, Comment comment)
        {
            try
            {
                Apartment a = context.Apartments.Where(w => w.ID == iD).FirstOrDefault();
                a.Comments.Add(comment);
                context.Apartments.Attach(a);
                context.SaveChanges();
                return a;
            }
            catch
            {
                return null;
            }
        }

        public Comment EditComment(Comment comment)
        {
            try
            {
                context.Comments.Attach(comment);
                context.Entry(comment).State = System.Data.Entity.EntityState.Modified;
                context.SaveChanges();
                return comment;
            }
            catch
            {
                return null;
            }
        }
    }
}