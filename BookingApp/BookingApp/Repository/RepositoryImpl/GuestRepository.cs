﻿using BookingApp.Models.Entity;
using BookingApp.Repository.RepositoryImpl;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BookingApp.Repository
{
    public class GuestRepository:IGuestRepository
    {
        Context.Context context;
        public GuestRepository(Context.Context con)
        {
            context = con;
        }

        public List<Guest> ReadAll()
        {
            return context.Guests.ToList();
        }

        public List<Guest> GetSorted()
        {
            return context.Guests.OrderBy(item => item.Password).ToList();
        }
        public Guest WriteGuest(Guest guest)
        {
            try
            {
                Guest saveGuest=context.Guests.Add(guest);
                context.SaveChanges();
                return saveGuest;
                //return guest;
            }
            catch
            {
                return null;
            }
        }

        public Guest EditGuest(Guest guest)
        {
            try
            {
                context.Guests.Attach(guest);
                context.Entry(guest).State = System.Data.Entity.EntityState.Modified;
                context.SaveChanges();
                return guest;
            }
            catch
            {
                return null;
            }
        }

        public Guest Delete(int id)
        {
            try
            {
                Guest a = context.Guests.Find(id);
                context.Guests.Remove(a);
                context.SaveChanges();
                return a;
            }
            catch
            {
                return null;
            }
        }

    }
}