﻿using BookingApp.Models.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BookingApp.Repository
{
    public class HostRepository:IHostRepository
    {
        Context.Context context;
        public HostRepository(Context.Context con)
        {
            context = con;
        }

        public List<Host> ReadAll()
        {
            return context.Hosts.ToList();
        }
        public Host WriteHost(Host host)
        {
            try
            {
                Host h= context.Hosts.Add(host);
                context.SaveChanges();
                return h;
            }
            catch
            {
                return null;
            }
        }

        public Host EditHost(Host host)
        {
            try
            {
                context.Hosts.Attach(host);
                context.Entry(host).State = System.Data.Entity.EntityState.Modified;
                context.SaveChanges();
                return host;
            }
            catch
            {
                return null;
            }
        }

        public Host Delete(int id)
        {
            try
            {
                Host a = context.Hosts.Find(id);
                context.Hosts.Remove(a);
                context.SaveChanges();
                return a;
            }
            catch
            {
                return null;
            }
        }
    }
}