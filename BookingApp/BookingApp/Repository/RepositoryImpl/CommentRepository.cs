﻿using BookingApp.Models.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BookingApp.Repository.RepositoryImpl
{
    public class CommentRepository : ICommentRepository
    {
        Context.Context context;

        public CommentRepository(Context.Context ct)
        {
            context = ct;
        }

        public List<Comment> ReadAll()
        {
            return context.Comments.ToList();
        }

        public List<Comment> ReadForApartment(Apartment apartment)
        {
            List<Comment> comentList=new List<Comment>();
            foreach(Comment c in context.Comments.ToList())
            {
                if (c.Apartment == apartment)
                {
                    comentList.Add(c);
                }
            }
            return comentList;
        }

        public Comment WriteComment(Comment comment)
        {
            try
            {
               
                Comment NewComment = context.Comments.Add(comment);
                context.SaveChanges();
                return NewComment;
            }
            catch
            {
                return null;
            }
        }
    }
}