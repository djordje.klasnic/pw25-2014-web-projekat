﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;
using BookingApp.Models.Entity;

namespace BookingApp.Repository.RepositoryImpl
{
    public class LocationRepository : ILocationRepository
    {
         Context.Context context;
        public LocationRepository(Context.Context con)
        {
            context = con;
        }

        
        public Location GetById(string ID)
        {
            Location returnLoc = new Location();
           List<Location> locations = context.Locations.ToList(); 
            foreach(Location l in locations)
            {
                if (l.Id == Int32.Parse(ID))
                {
                    return l;
                }
            }
            return null;
        }
    

        public List<Location> ReadAll()
        {
            return context.Locations.ToList();
        }
    }
}