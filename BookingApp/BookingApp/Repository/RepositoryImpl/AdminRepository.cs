﻿using BookingApp.Models.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BookingApp.Repository
{

    public class AdminRepository:IAdminRepository
    {
        Context.Context context;
        public AdminRepository(Context.Context con)
        {
            context = con;
        }

        public List<Admin> ReadAll()
        {
            
            return context.Admins.ToList();
        }

        public Admin WriteAdmin(Admin admin)
        {
            try
            {
                Admin a= context.Admins.Add(admin);
                return a;
            }
            catch
            {
                return null;
            }
        }

        public Admin EditAdmin(Admin admin)
        {
            try
            {
                context.Admins.Attach(admin);
                context.Entry(admin).State = System.Data.Entity.EntityState.Modified;
                context.SaveChanges();
                return admin;
            }
            catch
            {
                return null;
            }
        }

        public Admin Delete(int id)
        {
            try
            {
                Admin a = context.Admins.Find(id);
                context.Admins.Remove(a);
                context.SaveChanges();
                return a;
            }
            catch
            {
                return null;
            }
        }
    }
}