﻿using BookingApp.Models.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BookingApp.Repository
{
    public interface IHostRepository
    {
        List<Host> ReadAll();
        Host WriteHost(Host host);
        Host EditHost(Host host);
        Host Delete(int id);
    }
}
