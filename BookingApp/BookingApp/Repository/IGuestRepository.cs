﻿using BookingApp.Models.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BookingApp.Repository.RepositoryImpl
{
    public interface IGuestRepository
    {
        List<Guest> ReadAll();
        Guest WriteGuest(Guest guest);
        Guest EditGuest(Guest guest);
        List<Guest> GetSorted();
        Guest Delete(int id);
    }
}
