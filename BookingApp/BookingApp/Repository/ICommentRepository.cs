﻿using BookingApp.Models.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BookingApp.Repository
{
    public interface ICommentRepository
    {
        List<Comment> ReadAll();
        List<Comment> ReadForApartment(Apartment apartment);

        Comment WriteComment(Comment comment);

    }
}
