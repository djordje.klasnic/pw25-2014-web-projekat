﻿using BookingApp.Models.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BookingApp.Repository
{
    public interface IAdminRepository
    {
        List<Admin> ReadAll();
        Admin WriteAdmin(Admin admin);
        Admin EditAdmin(Admin admin);
        Admin Delete(int id);
    }
}
