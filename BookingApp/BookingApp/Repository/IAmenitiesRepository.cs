﻿using BookingApp.Models.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BookingApp.Repository
{
    public interface IAmenitiesRepository
    {
        List<Amenities> ReadAll();
        Amenities AddAmenities(Amenities amen);
        Amenities EditAmenities(Amenities amen);
        Amenities Delete(int id);
    }
}
