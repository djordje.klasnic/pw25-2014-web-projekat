﻿using BookingApp.Models.Entity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace BookingApp.Context
{
    public class Context: System.Data.Entity.DbContext
    {
        public System.Data.Entity.DbSet<Address> Addresses { get; set; }
        public System.Data.Entity.DbSet<Admin> Admins { get; set; }
        public System.Data.Entity.DbSet<Amenities> Amenities { get; set; }
        public System.Data.Entity.DbSet<Apartment> Apartments { get; set; }
        public System.Data.Entity.DbSet<Comment> Comments { get; set; }
        public System.Data.Entity.DbSet<Guest> Guests { get; set; }
        public System.Data.Entity.DbSet<Host> Hosts { get; set; }
        public System.Data.Entity.DbSet<Location> Locations { get; set; }
        public System.Data.Entity.DbSet<Reservation> Reservations { get; set; }

        public Context()
            :base("name=Context2")
        {
            

        }
    }
}