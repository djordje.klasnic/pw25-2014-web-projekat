﻿using BookingApp.Models.DTO;
using BookingApp.Models.Entity;
using BookingApp.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Mvc;

namespace BookingApp.Controllers
{
    public class ApartmentController : ApiController
    {
        public IApartmentRepository apartmentRepository;
        public ICommentRepository commentRepository;

        public ApartmentController(IApartmentRepository au, ICommentRepository cr)
        {
            apartmentRepository = au;
            commentRepository = cr;
        }



        
        [System.Web.Http.HttpGet]
        [System.Web.Http.Route("api/getapartments")]
        [ResponseType(typeof(List<Apartment>))]
        public IHttpActionResult Get()
        {
            List<Apartment> list = apartmentRepository.ReadAll();

            if (list == null)
            {

                return NotFound();
            }
            return Ok(list);
        }

        [System.Web.Http.HttpGet]
        [System.Web.Http.Route("api/getbyid/{ID}")]
        [ResponseType(typeof(Apartment))]
        public IHttpActionResult GetbyId(string ID)
        {
            
            Apartment a = apartmentRepository.getByid(Int32.Parse(ID));

            if (a == null)
            {
                return NotFound();
            }
            ApartmentDTO ap = new ApartmentDTO(a); 
            return Ok(ap);
        }

        //[System.Web.Http.HttpGet]
        //[System.Web.Http.Route("api/getbyiduser/{ID}")]
        //[ResponseType(typeof(Apartment))]
        //public IHttpActionResult getByidUser(string ID)
        //{

        //    Apartment a = apartmentRepository.getByid(Int32.Parse(ID));

        //    if (a == null)
        //    {
        //        return NotFound();
        //    }
        //    return Ok(a);
        //}

        [System.Web.Http.HttpGet]
        [System.Web.Http.Route("api/apartments/active")]
        public IHttpActionResult GetActive()
        {
            List<Apartment> list = apartmentRepository.ReadActive();

            if (list == null)
            {
                return NotFound();
            }
            return Ok(list);
        }

        [System.Web.Http.HttpGet]
        [System.Web.Http.Route("api/apartments/host/{id}")]
        public IHttpActionResult HostApartments(String id)
        {
            List<Apartment> list = apartmentRepository.ReadHostApartments(Int32.Parse(id));

            if (list == null)
            {
                return NotFound();
            }
            return Ok(list);
        }

        [System.Web.Http.HttpGet]
        [System.Web.Http.Route("api/apartments/active/host/{id}")]
        public IHttpActionResult HostActiveApartments(String id)
        {
            List<Apartment> list = apartmentRepository.ReadActiveHostApartments(Int32.Parse(id));

            if (list == null)
            {
                return NotFound();
            }
            return Ok(list);
        }

        [System.Web.Http.HttpGet]
        [System.Web.Http.Route("api/apartments/non-active/host/{id}")]
        public IHttpActionResult HostInactiveApartments(String id)
        {
            List<Apartment> list = apartmentRepository.ReadNonActiveHostApartments(Int32.Parse(id));

            if (list == null)
            {
                return NotFound();
            }
            return Ok(list);
        }

        
        [System.Web.Http.HttpPost]
        [System.Web.Http.Route("api/add-apartments")]
        [ResponseType(typeof(Apartment))]
        public IHttpActionResult AddApartment(Apartment apartment)
        {
            Apartment a = apartmentRepository.WriteApartment(apartment);
            if (a == null)
            {
                return NotFound();
            }
            return Ok(a);
        }

        public IHttpActionResult EditApartment(Apartment apartment)
        {
            Apartment a = apartmentRepository.EditApartment(apartment);
            if (a == null)
            {
                return NotFound();
            }
            return Ok(a);
        }

        public IHttpActionResult RemoveApartment(Apartment apartment)
        {
            Apartment a = apartmentRepository.RemoveApartment(apartment);
            if (a == null)
            {
                return NotFound();
            }
            return Ok(a);
        }

        [System.Web.Http.HttpPost]
        [System.Web.Http.Route("api/apartments/{apartmentId}/comments")]
        [ResponseType(typeof(Apartment))]
        public IHttpActionResult AddComment(string apartmentId, Comment comment)
        {
            Comment a = commentRepository.WriteComment(comment);
            Apartment apartment = apartmentRepository.addComment(Int32.Parse(apartmentId), comment);
            
            if (a == null)
            {
                return NotFound();
            }
            return Ok(a);
        }

        public IHttpActionResult EditComment(Comment comment)
        {
            Comment a = apartmentRepository.EditComment(comment);
            if (a == null)
            {
                return NotFound();
            }
            return Ok(a);
        }
    }
}