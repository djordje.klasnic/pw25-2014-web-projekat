﻿using BookingApp.Models.Entity;
using BookingApp.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Mvc;

namespace BookingApp.Controllers
{
    public class LocationController : ApiController

    {
        // GET: Location
        public ILocationRepository locationRepository;


        
        public LocationController(ILocationRepository lR)
        {
            locationRepository = lR;
        }

        [System.Web.Http.HttpGet]
        [System.Web.Http.Route("api/location/{ID}")]
        [ResponseType(typeof(Apartment))]
        public IHttpActionResult Get(string ID)
        {
            Location loc = locationRepository.GetById(ID);

            if (loc == null)
            {
                return NotFound();
            }
            return Ok(loc);
        }
    }
}