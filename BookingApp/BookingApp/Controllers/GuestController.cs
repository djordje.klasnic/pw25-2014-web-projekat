﻿using BookingApp.Models.Entity;
using BookingApp.Repository;
using BookingApp.Repository.RepositoryImpl;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Mvc;

namespace BookingApp.Controllers
{
    public class GuestController : ApiController
    {
        IGuestRepository guestRepository;
        public GuestController(IGuestRepository gR)
        {
            guestRepository = gR;
        }

        [System.Web.Http.HttpGet]
        [System.Web.Http.Route("api/getguest")]
        [ResponseType(typeof(List<Guest>))]
        public IHttpActionResult Get()
        {
            List<Guest> guests = guestRepository.ReadAll();
            if (guests != null)
            {

                return Ok(guests);

            }
            return NotFound();
        }

        public IHttpActionResult Post(Guest guest)
        {
            guest.Role = 1;
            Guest g = guestRepository.WriteGuest(guest);
            if (g == null)
                return Conflict();
            return Ok(g);
        }


        [System.Web.Http.HttpPut]
        [System.Web.Http.Route("api/editGuest")]
        [ResponseType(typeof(User))]
        public IHttpActionResult Put(Guest guest)
        {
            Guest g = guestRepository.EditGuest(guest);
            if (g == null)
                return Conflict();
            return Ok(g);
        }

        //public IHttpActionResult Get(int id)
        //{
        //    List<Guest> guests = guestRepository.ReadAll();
        //    if (guests != null)
        //    {
        //        foreach (Guest g in guests)
        //        {
        //            if (g.ID == id)
        //                return Ok(g);
        //        }
        //    }
        //    return NotFound();
        //}




        [System.Web.Http.HttpGet]
        [System.Web.Http.Route("api/sortguest")]
        [ResponseType(typeof(List<Guest>))]
        public IHttpActionResult ReadSorted()
        {
            List<Guest> guests = guestRepository.GetSorted();
            if (guests != null)
            {
                
                        return Ok(guests);
                
            }
            return NotFound();
        }

        [System.Web.Http.HttpDelete]
        [System.Web.Http.Route("api/deleteguest/{id}")]
        public IHttpActionResult DeleteGuest(int id)
        {
            List<Guest> guests = guestRepository.ReadAll();
            if (guests != null)
            {
                foreach (Guest a in guests)
                {
                    if (a.ID == id)
                    {
                        guestRepository.Delete(id);
                        return Ok(a);
                    }
                }
            }
            return NotFound();
        }

    }
}