﻿using BookingApp.Models.Entity;
using BookingApp.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Mvc;

namespace BookingApp.Controllers
{
    public class HostController : ApiController
    {
        IHostRepository hostRepository;
        public HostController(IHostRepository hR)
        {
            hostRepository = hR;
        }

        [System.Web.Http.HttpPut]
        [System.Web.Http.Route("api/editHost")]
        [ResponseType(typeof(User))]
        public IHttpActionResult Put(Host host)
        {
            Host h = hostRepository.EditHost(host);
            if (h == null)
                return Conflict();
            return Ok(h);
        }

        public IHttpActionResult Get(int id)
        {
            List<Host> hosts = hostRepository.ReadAll();
            if (hosts != null)
            {
                foreach (Host g in hosts)
                {
                    if (g.ID == id)
                        return Ok(g);
                }
            }
            return NotFound();
        }


        [System.Web.Http.HttpPost]
        [System.Web.Http.Route("api/addhost")]
        [ResponseType(typeof(User))]
        public IHttpActionResult Post(Host host)
        {
            host.Role = 2;
            Host h = hostRepository.WriteHost(host);
            if (h == null)
                return Conflict();
            return Ok(h);
        }

        [System.Web.Http.HttpDelete]
        [System.Web.Http.Route("api/deletehost/{id}")]
        public IHttpActionResult DeleteHost(int id)
        {
            List<Host> hosts = hostRepository.ReadAll();
            if (hosts != null)
            {
                foreach (Host a in hosts)
                {
                    if (a.ID == id)
                    {
                        hostRepository.Delete(id);
                        return Ok(a);
                    }
                }
            }
            return NotFound();
        }

    }
}