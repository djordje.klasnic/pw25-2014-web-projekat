﻿using BookingApp.Models.Entity;
using BookingApp.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Mvc;

namespace BookingApp.Controllers
{
    public class AdminController : ApiController
    {
        IAdminRepository adminRepository;
        public AdminController(IAdminRepository aR)
        {
            adminRepository = aR;
        }

        [System.Web.Http.HttpPut]
        [System.Web.Http.Route("api/editAdmin")]
        [ResponseType(typeof(User))]
        public IHttpActionResult Put(Admin admin)
        {
            Admin a = adminRepository.EditAdmin(admin);
            if (a == null)
                return Conflict();
            return Ok(a);
        }

        [System.Web.Http.HttpGet]
        [System.Web.Http.Route("api/getadmins")]
        [ResponseType(typeof(List<Admin>))]
        public IHttpActionResult Get()
        {
            List<Admin> a = adminRepository.ReadAll();
            if (a == null)
                return Conflict();
            return Ok(a);
        }


        // GET api/values/5editAdmin
        public IHttpActionResult Get(int id)
        {
            List<Admin> admins = adminRepository.ReadAll();
            if (admins != null)
            {
                foreach (Admin a in admins)
                {
                    if (a.ID == id)
                        return Ok(a);
                }
            }
            return NotFound();
        }

        [System.Web.Http.HttpDelete]
        [System.Web.Http.Route("api/deleteadmin/{id}")]
        public IHttpActionResult DeleteAdmin(int id)
        {
            List<Admin> admins = adminRepository.ReadAll();
            if (admins != null)
            {
                foreach (Admin a in admins)
                {
                    if (a.ID == id)
                    {
                        adminRepository.Delete(id);
                        return Ok(a);
                    }
                }
            }
            return NotFound();
        }
    }
}