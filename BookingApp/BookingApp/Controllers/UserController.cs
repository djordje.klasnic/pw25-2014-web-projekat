﻿using BookingApp.Context;
using BookingApp.Models.Entity;
using BookingApp.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Mvc;
using BookingApp.Models.DTD;
using BookingApp.Repository.RepositoryImpl;

namespace BookingApp.Controllers
{
    public class UserController : ApiController
    {
        IAdminRepository adminRepository;
        IGuestRepository guestRepository;
        IHostRepository hostRepository;
        
        public UserController(IAdminRepository aR,IGuestRepository gR, IHostRepository hR)
        {
            adminRepository = aR;
            guestRepository = gR;
            hostRepository = hR;
        }


        [System.Web.Http.HttpGet]
        [System.Web.Http.Route("api/returnallusers")]
        [ResponseType(typeof(List<User>))]
        public IHttpActionResult Get()
        {
            List<Admin> admins = adminRepository.ReadAll();
            List<Guest> guests = guestRepository.ReadAll();
            List<Host> hosts = hostRepository.ReadAll();

       

            List<User> list = new List<User>();
            foreach(Admin a in admins)
            {
                list.Add((User)a);
            }
            foreach (Guest g in guests)
            {
                list.Add((User)g);
            }
            foreach (Host h in hosts)
            {
                list.Add((User)h);
            }

            if (list == null)
            {
                return NotFound();
            }
            return Ok(list);
        }

        [System.Web.Http.HttpPost]
        [System.Web.Http.Route("api/register")]
        [ResponseType(typeof(User))]
        public IHttpActionResult Post(Guest guest)
        {
            Guest g = guestRepository.WriteGuest(guest);
            if (g == null)
                return Conflict();
            return Ok(g);
        }

        [System.Web.Http.HttpPost]
        [System.Web.Http.Route("api/login")]
        [ResponseType(typeof(User))]
        public IHttpActionResult Login(LoginDTO loginUser)
        {
            User logUser = null;
            List<Admin> admins = adminRepository.ReadAll();
            foreach (Admin admin in admins)
                {
                    if (admin.Username == loginUser.Username && admin.Password == loginUser.Password)
                    {
                        logUser = (User)admin;
                        logUser.Role = 0;
                        return Ok(logUser);
                    }
                }
            List<Guest> guests = guestRepository.ReadAll();
            foreach (Guest g in guests)
            {
                if (g.Username == loginUser.Username && g.Password == loginUser.Password)
                {
                    logUser = (User)g;
                    logUser.Role = 1;
                    return Ok(logUser);
                }
            }
            List<Host> hosts = hostRepository.ReadAll();
            foreach (Host h in hosts)
            {
                if (h.Username == loginUser.Username && h.Password == loginUser.Password)
                {
                    logUser = (User)h;
                    logUser.Role = 2;
                    return Ok(logUser);
                }
            }
            return NotFound();
        }

    }
}