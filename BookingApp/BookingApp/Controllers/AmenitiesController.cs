﻿using BookingApp.Models.Entity;
using BookingApp.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Mvc;

namespace BookingApp.Controllers
{
    public class AmenitiesController : ApiController
    {
        IAmenitiesRepository amenitiesRepository;
        public AmenitiesController(IAmenitiesRepository aR)
        {
            amenitiesRepository = aR;
        }

        [System.Web.Http.HttpPut]
        [System.Web.Http.Route("api/amenities")]
        [ResponseType(typeof(Amenities))]
        public IHttpActionResult Put(Amenities amen)
        {
            Amenities a = amenitiesRepository.EditAmenities(amen);
            if (a == null)
                return Conflict();
            return Ok(a);
        }

        [System.Web.Http.HttpGet]
        [System.Web.Http.Route("api/amenities")]
        [ResponseType(typeof(List<Amenities>))]
        public IHttpActionResult Get()
        {
            List<Amenities> amenities = amenitiesRepository.ReadAll();
            if (amenities == null)
                return Conflict();
            return Ok(amenities);
        }


        [System.Web.Http.HttpPost]
        [System.Web.Http.Route("api/amenities")]
        public IHttpActionResult Post(Amenities amenitie)
        {
            Amenities g = amenitiesRepository.AddAmenities(amenitie);
            if (g == null)
                return Conflict();
            return Ok(g);
        }

        [System.Web.Http.HttpDelete]
        [System.Web.Http.Route("api/amenities/{id}")]
        public IHttpActionResult Delete(int id)
        {
            List<Amenities> amenities = amenitiesRepository.ReadAll();
            if (amenities != null)
            {
                foreach (Amenities a in amenities)
                {
                    if (a.ID == id)
                    {
                        amenitiesRepository.Delete(id);
                        return Ok(a);
                    }
                }
            }
            return NotFound();
        }
    }
}