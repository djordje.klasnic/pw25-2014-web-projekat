﻿using BookingApp.Models.Entity;
using BookingApp.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;

namespace BookingApp.Controllers
{
    public class CommentController : ApiController
    {
        public ICommentRepository commentRepository;

        public CommentController(ICommentRepository cR)
        {
            commentRepository = cR;
        }

        public IHttpActionResult Get()
        {
            List<Comment> list = commentRepository.ReadAll();

            if (list == null)
            {
                return NotFound();
            }
            return Ok(list);
        }
    }
}