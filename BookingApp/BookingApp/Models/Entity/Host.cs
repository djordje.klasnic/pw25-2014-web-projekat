﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BookingApp.Models.Entity
{
    public class Host:User
    {
        public List<Apartment> RentingApartments { get; set; }

        public Host()
        {

        }
        public Host(int iD, string username, string password, string firstName, string lastName, Gender gender, int Role, List<Apartment> rentingApartments) : base(iD, username, password, firstName, lastName, gender, Role)
        {
            RentingApartments = rentingApartments;
        }
    }
}