﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BookingApp.Models.Entity
{
    public class Admin : User
    {
        public Admin()
        {
        }

        public Admin(int iD, string username, string password, string firstName, string lastName, Gender gender,int Role) : base(iD, username, password, firstName, lastName, gender, Role)
        {
        }
    }
}