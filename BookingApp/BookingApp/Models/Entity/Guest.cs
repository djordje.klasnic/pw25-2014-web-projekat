﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BookingApp.Models.Entity
{
    public class Guest:User 
    {
        public List<Reservation> Reservations { get; set; }


       

        public Guest()
        {
        }

        public Guest(int iD, string username, string password, string firstName, string lastName, Gender gender, int Role,  List<Reservation> reservations) : base(iD, username, password, firstName, lastName, gender,  Role)
        {
            Reservations = reservations;
        }
    }
}