﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace BookingApp.Models.Entity
{
    public class Amenities
    {
        public int ID { get; set; }
        public string Description { get; set; }
        [ForeignKey("Apartment")]
        public int ApartmentId { get; set; }
        public Apartment Apartment { get; set; }

        public Amenities()
        {
        }

        public Amenities(int iD, string description)
        {
            ID = iD;
            Description = description;
        }

    }
}