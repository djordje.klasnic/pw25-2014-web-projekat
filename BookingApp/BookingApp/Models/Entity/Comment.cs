﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace BookingApp.Models.Entity
{
    public class Comment
    {
        public int Id { get; set; }
        public string Description { get; set; }
        public Guest Guest { get; set; }
        [ForeignKey("Apartment")]
        public int ApartmentId { get; set; }
        public Apartment Apartment { get; set; }

        public int Evaluation { get; set; }
        public int Status { get; set; }

        
        public Comment(int id, string description, Guest user, int evaluation,Apartment apartment,int appid)
        {
            Id = id;
            Description = description;
            Apartment = apartment;
            Guest = user;
            ApartmentId = appid;
            
            Evaluation = evaluation;
        }

        public Comment()
        {
        }
    }
}