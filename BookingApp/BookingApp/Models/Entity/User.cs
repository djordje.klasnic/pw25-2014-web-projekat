﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BookingApp.Models.Entity
{
    public class User
    {

        public int ID { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public Gender Gender { get; set; }
        public int Role { get; set; }

        public User()
        {
        }

        public User(int iD, string username, string password, string firstName, string lastName, Gender gender,int role)
        {
            this.ID = iD;
            this.Username = username;
            this.Password = password;
            this.FirstName = firstName;
            this.LastName = lastName;
            this.Gender = gender;
            this.Role = role;
        }

    }
}