﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace BookingApp.Models.Entity
{
    public class Apartment
    {
        [Key]
        public int ID { get; set; }
        public ApartmentType Type { get; set; }
        public int RoomNumber { get; set; }
        public int GuestNumber { get; set; }
        public Location Location { get; set; }
        public List<DateTime> RentDates { get; set; }
        public List<DateTime> AvailableRentDates { get; set; }
        [ForeignKey("Host")]
        public int HostId { get; set; }
        public Host Host { get; set; }
       
        public List<Comment> Comments { get; set; }
        public List<string> Pictures { get; set; }
        public int Price { get; set; }
        public DateTime SignUpTime { get; set; }
        public DateTime CheckOutTime { get; set; }
        public Status Status { get; set; }
        public List<Amenities> Amenities { get; set; }
        public List<Reservation> Reservations { get; set; }

        public Apartment()
        {
            RentDates = new List<DateTime>();
            AvailableRentDates = new List<DateTime>();
            Comments = new List<Comment>();
            Pictures = new List<string>();
            Amenities = new List<Amenities>();
            Reservations = new List<Reservation>();
            Host = new Host();
            Location = new Location();
        }

        public Apartment(int id, ApartmentType type, int roomNumber, int guestNumber, Location location, List<DateTime> rentDates, List<DateTime> availableRentDates, Host host, List<Comment> comment, List<string> pictures, int price, DateTime signUpTime, DateTime checkOutTime, Status status, List<Amenities> amenities, List<Reservation> reservations)
        {
            ID = id;
            Type = type;
            RoomNumber = roomNumber;
            GuestNumber = guestNumber;
            Location = location;
            RentDates = rentDates;
            AvailableRentDates = availableRentDates;
            Host = host;
            Comments = comment;
            Pictures = pictures;
            Price = price;
            SignUpTime = signUpTime;
            CheckOutTime = checkOutTime;
            Status = status;
            Amenities = amenities;
            Reservations = reservations;
        }
    }
}