﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BookingApp.Models.Entity
{
    public class Location
    {
        public int Id { get; set; }
        public Address Address { get; set; }
        public int XCoordinate { get; set; }
        public int YCoordinate { get; set; }




        public Location(int id, Address address, int xCoordinate, int yCoordinate)
        {
            Id = id;
            Address = address;
            XCoordinate = xCoordinate;
            YCoordinate = yCoordinate;
        }

        public Location()
        {
        }
    }
}