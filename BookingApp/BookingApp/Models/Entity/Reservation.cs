﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace BookingApp.Models.Entity
{
    public class Reservation
    {
        public int ID { get; set; }
        public Apartment ReservedApartment { get; set; }
        public DateTime CheckInDate { get; set; }
        public int NumberOfNights { get; set; }
        public int TotalPrice { get; set; }
        [ForeignKey("Guest")]
        public int GuestId { get; set; }
        public Guest Guest { get; set; }
        public StatusOFReservation Status { get; set; }

        public Reservation()
        {
        }

        public Reservation(int iD, Apartment reservedApartment, DateTime checkInDate, int numberOfNights, int totalPrice, Guest guest, StatusOFReservation status)
        {
            ID = iD;
            ReservedApartment = reservedApartment;
            CheckInDate = checkInDate;
            NumberOfNights = numberOfNights;
            TotalPrice = totalPrice;
            Guest = guest;
            Status = status;
        }

        
    }
}