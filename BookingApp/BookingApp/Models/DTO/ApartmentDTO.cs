﻿using BookingApp.Models.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BookingApp.Models.DTO
{
    public class ApartmentDTO
    {
        public int ID { get; set; }
        public Entity.ApartmentType Type { get; set; }
        public int RoomNumber { get; set; }
        public int GuestNumber { get; set; }
        public Location Location { get; set; }
        public List<DateTime> RentDates { get; set; }
        public List<DateTime> AvailableRentDates { get; set; }
        public int HostId { get; set; }
        public Host Host { get; set; }

        public List<Comment> Comments { get; set; }
        public List<string> Pictures { get; set; }
        public int Price { get; set; }
        public DateTime SignUpTime { get; set; }
        public DateTime CheckOutTime { get; set; }
        public Status Status { get; set; }
        public List<Amenities> Amenities { get; set; }
        public List<Reservation> Reservations { get; set; }

        public ApartmentDTO()
        {
            RentDates = new List<DateTime>();
            AvailableRentDates = new List<DateTime>();
            Comments = new List<Comment>();
            Pictures = new List<string>();
            Amenities = new List<Amenities>();
            Reservations = new List<Reservation>();
        }

        public ApartmentDTO(Apartment apartment)
        {
            ID = apartment.ID;
            Type = apartment.Type;
            RoomNumber = apartment.RoomNumber;
            GuestNumber = apartment.GuestNumber;
            Location = apartment.Location;
            RentDates = apartment.RentDates;
            AvailableRentDates = apartment.AvailableRentDates;
            Host = apartment.Host;
            Comments = apartment.Comments;
            Pictures = apartment.Pictures;
            Price = apartment.Price;
            SignUpTime = apartment.SignUpTime;
            CheckOutTime = apartment.CheckOutTime;
            Status = apartment.Status;
            Amenities = apartment.Amenities;
            Reservations = apartment.Reservations;
        }
    }
}